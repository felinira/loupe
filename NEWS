45.beta.2
=======

- Always ask which program to use for "Open With…"
- SVG: Force minimum tile size of 1000x1000
- SVG: Don't render first tiles with zoom=1
- Update screenshots

45.beta.1
=======

- Reload images when the file is replaced by moving another file at it's place
- Update dependencies

45.beta
=======
- Newly designed print dialog
- Added sandboxing for SVG rendering by using glycin
- Use RaisedBorder for HeaderBar to avoid shadow over images
- Fix browsing images on SMB shares not working
- Fix touchscreen gestures not working when zoomed in
- Fix touchscreen swipe only working on second attempt
- Added JpegXL support via new glycin version
- Added overshoot for swipe gesture
- New custom style for drop targets
- Changed name to Image Viewer

45.alpha
=======
- Add help
- Disable trashing for non-native files
- Give toast for latest deleted file priority over the others
- Only list supported image formats in file chooser
- Do not guess a potentially different background color for opaque images
- Fix incorrect updates when deleting or changing images
- Fix SVG zooming with high-res scroll-wheels
- Use glycin for sandboxed extendable image loading expect for SVG
- Remove backspace as a trash shortcut
- Change command line to open all given images in one window
- Do not show "Set as Background" if operation was aborted
- Let the user select multiple files to open in file chooser
- Support forward and back mouse buttons
- Correctly align right-click menu

44.3
=======
- Hide cursor together with controls when fullscreened
- Remove ctrl+x shortcut for deleting images
- Fix 'delete' action appearing too long in help overlay
- Split rotate actions making it possible to use them in help overlay
- Cancel GFile loading when decoder is dropped
- Make crossfade from spinner page to image work
- Don't make activatable row subtitles selectable in properties view
- Disable "open folder" action for non-native files (GTK doesn't support it)
- Leave fullscreen when status page is shown
- Skip unsupported formats when sliding through images
- Show window after 2 seconds if dimension are not yet known
- Cancel scroll deceleration on gesture use
- Hide headerbar on idle in fullscreen
- Show controls on mouse click
- Better solution for not registering swipes as cursor motion
- Use AdwBreakpoint to move properties below the image for narrow windows
- Fix vertical minimum size in measure for LpImageView
- Ignore EXIF rotation info for now in HEIF formats

44.2
=======
- Use GFile instead of direct file access
- Do not crash when monitor info is not available
- Get rid of many unwraps and report issues to UI instead, just to be safe
- #130 Fix displayed times are wrong because they do not respect timezones
- Add basic support for ICC color profiles
- Support opening multiple files via drag and drop
- Fix textures and background color not updating when image data changed
- #124 Fix not defaulting to light background color in light theme
- Fix thumbnail is broken for auto-rotated (EXIF) images
- #120 Fix high-res scroll wheel issues
- #134 Fix 100%/200% shortcuts ending up at best-fit when window size close to 100%/200%
- Add printing support
- Center new tilings in the viewport, making the first rendered tile cover the viewport already
- Allow dragging (panning) the image with middle button
- Generate list of mime types in meson and fix list of formats
- Set actions enabled state based on UI property status, not manually everywhere
- Add and update license headers with script based on commit info
- Hide overlay controls after inactivity
- Remove OSD from HeaderBar in fullscreen
- Drop AdwFlap for HeaderBar
- Make properties button insensitive if no image shown/loading
- Move 'toggle fullscreen' button from inner to outer position
- Add 'all files' filter option to file chooser

44.1
=======
Bugfix release after initial feedback:
 - Bump GTK dependency to 4.11.1 because of required fixes for scaled texture
 - Drop gtk-macros (crate) dependency since it is not needed and lacking a LICENSE files
 - #115 Fix WebP support for still images
 - #115 Fix not detecting HEIC files

44.0
=======

Initial release